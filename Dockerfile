# Utiliser une image Apache avec PHP
FROM php:apache

# Copier le fichier index.php dans le répertoire web d'Apache
COPY app/index.php /var/www/html/

# Exposer le port 80 pour accéder au serveur web
EXPOSE 80

